# Cursed Images Mod(1.12.2)

## Info
This is a Minecraft mod for 1.12.2. It adds the following features:

- Creamy Creeper: 
    - Makes weird sounds
    - Nuzzles you
    - 69 health (half hearts)

### Planned Features:

- Creamy Creeper:
    - Tameable?

## Dependencies
[CreepinoUtils](https://gitlab.com/creepino-creations/creepino-utils-mod)