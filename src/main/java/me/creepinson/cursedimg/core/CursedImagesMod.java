package me.creepinson.cursedimg.core;

import me.creepinson.creepinoutils.base.BaseMod;
import me.creepinson.cursedimg.entity.EntityCreamyCreeper;
import me.creepinson.cursedimg.entity.render.RenderCreamyCreeper;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;

/**
 * @author Creepinson http://gitlab.com/creepinson
 * Project forge-mod-template
 **/

@Mod(modid = CursedImagesMod.MOD_ID, name = CursedImagesMod.MOD_NAME, version = CursedImagesMod.MOD_VERSION, dependencies="required-after:creepinoutils")
public class CursedImagesMod extends BaseMod {
    public static final String MOD_ID = "cursedimg", MOD_ID_SHORT = "cursedimg", MOD_NAME = "Cursed Images", MOD_URL = "", MOD_VERSION = "1.0.1", MOD_DEPENDENCIES = "";
    public static final boolean DEBUG = true; // DEFAULT = false

    // TODO: make creativecore not a requirement

    @Mod.Instance(CursedImagesMod.MOD_ID)
    private static CursedImagesMod INSTANCE;

    public static CursedImagesMod getInstance() {
        return INSTANCE;
    }

    @SidedProxy(clientSide = "me.creepinson.cursedimg.core.ClientProxy", serverSide = "me.creepinson.cursedimg.core.CommonProxy")
    public static CommonProxy proxy;

    public CursedImagesMod() {
        super(MOD_URL, null, MOD_ID, MOD_NAME, MOD_VERSION);
    }

    public static void debug(String s) {
        if(getInstance().debug) getInstance().getLogger().info("DEBUG -- " + s);
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        super.preInit(event, null);
        if (event.getSide() == Side.CLIENT) {
            debug("CLIENT PRREINIT");
            RenderingRegistry.registerEntityRenderingHandler(EntityCreamyCreeper.class, RenderCreamyCreeper::new);
        }
//        EasyRegistry.registerBlockWithItem(BlockHandler.ANIMATION_TEST_BLOCK, new ResourceLocation(MOD_ID, "animation_test_block"));

    }

    @Mod.EventHandler
    @Override
    public void init(FMLInitializationEvent event) {
        super.init(event);
//        GameRegistry.registerTileEntity(TileEntityAnimationTest.class, new ResourceLocation(MOD_ID, "tile_animation_test"));
    }

    @Mod.EventHandler
    @Override
    public void postInit(FMLPostInitializationEvent event) {
        super.postInit(event);
    }
}
