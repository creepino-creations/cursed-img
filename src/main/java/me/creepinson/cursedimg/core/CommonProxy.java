package me.creepinson.cursedimg.core;

import me.creepinson.creepinoutils.base.BaseProxy;

public class CommonProxy extends BaseProxy {

	@Override
	public void registerBlocks() {
		
	}

	@Override
	public void registerItems() {
		
	}

	@Override
	public void registerTileEntities() {
		
	}

	@Override
	public void registerRecipes() {
		
	}

	@Override
	public void registerPackets() {
		
	}

}
