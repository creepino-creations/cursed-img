package me.creepinson.cursedimg.handler;

import me.creepinson.cursedimg.core.CursedImagesMod;
import me.creepinson.cursedimg.entity.EntityCreamyCreeper;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.EntityEntry;
import net.minecraftforge.fml.common.registry.EntityEntryBuilder;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

/**
 * @author Creepinson http://gitlab.com/creepinson
 * Project Tubes Plus
 **/
@Mod.EventBusSubscriber()
public class RegistryHandler {

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {

        event.getRegistry().registerAll();
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll();
    }

    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {

    }

    @SubscribeEvent
    public static void attachCapability(AttachCapabilitiesEvent<Entity> event) {

    }

    /**
     * Register the EntityPanda class using the EntityEntryBuilder
     * Some details are here: https://github.com/MinecraftForge/MinecraftForge/pull/4408
     * The class is here: https://github.com/MinecraftForge/MinecraftForge/blob/1.12.x/src/main/java/net/minecraftforge/fml/common/registry/EntityEntryBuilder.java
     * It's already gone in 1.13.x
     * @param event
     */
    @SubscribeEvent
    public static void entityRegistration(RegistryEvent.Register<EntityEntry> event) {
        event.getRegistry().register(EntityEntryBuilder.create().entity(EntityCreamyCreeper.class)
                .id(new ResourceLocation(CursedImagesMod.getInstance().modId, "creamy_creeper"), 33).name("Creamy Creeper").tracker(160, 2, false)
                .egg(0x4c3e30, 0xf0f0f)
                .spawn(EnumCreatureType.CREATURE, 4, 2, 8, ForgeRegistries.BIOMES.getValuesCollection()).build());
        CursedImagesMod.debug("Entity entries registered");
    }


}
