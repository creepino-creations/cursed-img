package me.creepinson.cursedimg.handler;

import me.creepinson.cursedimg.core.CursedImagesMod;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * @author Creepinson http://gitlab.com/creepinson
 * Project forge-template
 **/
@Mod.EventBusSubscriber
public class SoundHandler {
    private static final ResourceLocation CREAMY_CREEPER_AMBIENT_LOC = new ResourceLocation(CursedImagesMod.getInstance().modId, "creamy_creeper_ambient");
    public static final SoundEvent CREAMY_CREEPER_AMBIENT = new SoundEvent(CREAMY_CREEPER_AMBIENT_LOC).setRegistryName(CREAMY_CREEPER_AMBIENT_LOC);

    private static final ResourceLocation CREAMY_CREEPER_HURT_LOC = new ResourceLocation(CursedImagesMod.getInstance().modId, "creamy_creeper_hurt");
    public static final SoundEvent CREAMY_CREEPER_HURT = new SoundEvent(CREAMY_CREEPER_HURT_LOC).setRegistryName(CREAMY_CREEPER_HURT_LOC);


    @SubscribeEvent
    public static void register(RegistryEvent.Register<SoundEvent> event) {
        event.getRegistry().registerAll(CREAMY_CREEPER_AMBIENT, CREAMY_CREEPER_HURT);
    }
}


