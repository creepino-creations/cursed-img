package me.creepinson.cursedimg.entity;

import me.creepinson.cursedimg.core.CursedImagesMod;
import me.creepinson.cursedimg.handler.SoundHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

/**
 * @author Creepinson http://gitlab.com/creepinson
 * Project forge-template
 **/
public class EntityCreamyCreeper extends EntityLiving {
    private static final String[] MESSAGES = new String[]{"UwU", "OwO, what's this?", "*nuzzles*"};
    private static final double CHAT_RANGE = 5.0D;
    private int cooldown;
    private int maxCooldown = 400;

    public EntityCreamyCreeper(World worldIn) {
        super(worldIn);
        this.setSize(1.05F, 2.25F);

    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(69.0F);

    }

    @Nullable
    @Override
    protected SoundEvent getAmbientSound() {
        return SoundHandler.CREAMY_CREEPER_AMBIENT;
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return SoundHandler.CREAMY_CREEPER_HURT;
    }

    @Override
    public void onEntityUpdate() {
        super.onEntityUpdate();
        List<EntityPlayer> nearby = world.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().expand(CHAT_RANGE, CHAT_RANGE, CHAT_RANGE));
        for (EntityPlayer p : nearby) {
            if (cooldown == getRNG().nextInt(maxCooldown - 20)+15) {
                p.sendMessage(new TextComponentString("Creamy Creeper: " + MESSAGES[getRNG().nextInt(MESSAGES.length)]));
            }
        }
        if (cooldown == maxCooldown) {
            cooldown = 0;
        }

        cooldown++;
    }
}
