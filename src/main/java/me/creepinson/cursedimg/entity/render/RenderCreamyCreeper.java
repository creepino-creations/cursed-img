package me.creepinson.cursedimg.entity.render;

import me.creepinson.cursedimg.core.CursedImagesMod;
import me.creepinson.cursedimg.entity.EntityCreamyCreeper;
import me.creepinson.cursedimg.entity.render.model.ModelCreamyCreeper;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nullable;

/**
 * @author Creepinson http://gitlab.com/creepinson
 * Project forge-template
 **/
public class RenderCreamyCreeper extends RenderLiving<EntityCreamyCreeper> {

    public RenderCreamyCreeper(RenderManager rendermanagerIn) {
        super(rendermanagerIn, new ModelCreamyCreeper(), 0.5F);
    }

    @Override
    protected void preRenderCallback(EntityCreamyCreeper entity, float partialTickTime) {
        super.preRenderCallback(entity, partialTickTime);
        GlStateManager.scale(2D,  2.5D, 2D);
    }

    @Nullable
    @Override
    protected ResourceLocation getEntityTexture(EntityCreamyCreeper entity) {
        return new ResourceLocation(CursedImagesMod.getInstance().modId, "textures/entity/creamy_creeper.png");
    }
}
